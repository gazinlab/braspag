# GAZIN LAB
## Image content
### Maintainer eriktonon@gmail.com {github.com/eriktonon}
* PHP Version 7.3.10
* Apache Version 2.4
* Driver Postgres and Oracle
* SOAP
* Composer
* Virtualhost /var/www/html/public
* Xdebug
* OCI8
* Pecl
* TimeZone Brasil - SP
 

## Installation

```shell 
$ docker pull gazinlab/braspag:v1.0.0
$ docker run -d --name project -p 80:80 -v ${PWD}:/var/www/html/public gazinlab/braspag:v1.0.0
```